<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de Selección 2';
?>
<div class="site-index">
    
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de Selección</h1>
    </div>

    <div class="body-content">
        
        <div class="row">
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Número de ciclistas que hay</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta1a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta1'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
         <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p> Número de ciclistas que hay del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta2a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta2'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
                  <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Edad media de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta3a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta3'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
                     <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 4</h3>
                        <p>La edad media de los ciclistas del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta4a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta4'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
            
                   <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 5</h3>
                        <p>La edad media de los ciclistas por cada equipo</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta5a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta5'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
                     <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 6</h3>
                        <p>El número de ciclistas por equipo</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta6a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta6'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
                     <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 7</h3>
                        <p>El número total de puertos</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta7a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta7'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
                     <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 8</h3>
                        <p>El número total de puertos mayores de 1500</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta8a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta8'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
                     <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta9a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta9'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
                     <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 10</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta10a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta10'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
                     <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 11</h3>
                        <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta11a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta11'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 12</h3>
                        <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>
                        <p>
                            <?= Html::a('Contacto',['site/consulta12a'],['class' =>'btn btn-primary'])?>
                            <?= Html::a('Login',['site/consulta12'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
               </div>
        </div>
</div>
</div>
</div>
