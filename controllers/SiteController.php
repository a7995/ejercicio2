<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
     public function actionConsulta1(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT DISTINCT COUNT(dorsal) as Cantidad FROM ciclista'
            
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Cantidad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT DISTINCT COUNT(dorsal) as Cantidad FROM ciclista",
        ]);
     }
    public function actionConsulta1a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("COUNT(dorsal) as Cantidad")->distinct(),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Cantidad'],
            "titulo"=>"Consulta 1 con Action Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT DISTINCT COUNT(dorsal) as Cantidad FROM ciclista",
        ]);
}
    public function actionConsulta2(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT COUNT(dorsal) as TotalBanesto FROM ciclista WHERE nomequipo = "banesto"'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['TotalBanesto'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) as TotalBanesto FROM ciclista WHERE nomequipo = 'banesto'",
                ]);
    }
    public function actionConsulta2a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("COUNT(dorsal) as TotalBanesto")->distinct()->where('nomequipo = "banesto"'),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['TotalBanesto'],
            "titulo"=>"Consulta 2 con Action Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) as TotalBanesto FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
}
    public function actionConsulta3(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT AVG(edad) as EdadMedia FROM ciclista'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['EdadMedia'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) FROM ciclista",
                ]);
    }
     public function actionConsulta3a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("AVG(edad) as EdadMedia")->distinct(),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['EdadMedia'],
            "titulo"=>"Consulta 3 con Action Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) FROM ciclista",
        ]);
    }
    public function actionConsulta4(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT AVG(edad) as EdadMediaBanesto FROM ciclista WHERE nomequipo = "banesto"'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['EdadMediaBanesto'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) FROM ciclista WHERE nomequipo = 'banesto'",
                ]);
    }
    public function actionConsulta4a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("AVG(edad) as EdadMedia")->distinct()->where("nomequipo='banesto'"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['EdadMedia'],
            "titulo"=>"Consulta 4 con Action Record",
            "enunciado"=>"Edad media de los ciclistas de Banesto",
            "sql"=>"SELECT AVG(edad) FROM ciclista WHERE nomequipo = 'banesto' ",
        ]);
    }
    public function actionConsulta5(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT nomequipo, AVG (edad) as EdadMediaEquipo FROM  ciclista GROUP BY nomequipo'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'EdadMediaEquipo'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG (edad) FROM  ciclista GROUP BY nomequipo",
                ]);
    }
    public function actionConsulta5a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("nomequipo, AVG(edad) as EdadMediaEquipo")->groupBy('nomequipo'),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','EdadMediaEquipo'],
            "titulo"=>"Consulta 5 con Action Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG (edad) FROM  ciclista GROUP BY nomequipo",
        ]);
    }
    public function actionConsulta6(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT nomequipo, COUNT(dorsal) AS Cantidad FROM  ciclista GROUP BY nomequipo'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'Cantidad'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(dorsal) FROM  ciclista GROUP BY nomequipo",
                ]);
    }
    public function actionConsulta6a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("nomequipo, COUNT(dorsal) as Cantidad")->distinct()->groupBy("nomequipo"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'Cantidad'],
            "titulo"=>"Consulta 6 con Action Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(dorsal) FROM  ciclista GROUP BY nomequipo",
        ]);
    }
    public function actionConsulta7(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT COUNT(nompuerto) as Cantidad FROM puerto'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Cantidad'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(nompuerto) FROM puerto",
                ]);
    }
    public function actionConsulta7a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Puerto::find()->select("COUNT(nompuerto) as Cantidad")->distinct(),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Cantidad'],
            "titulo"=>"Consulta 7 con Action Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(nompuerto) FROM puerto",
        ]);
    }
    public function actionConsulta8(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT COUNT(nompuerto) as Cantidad FROM puerto WHERE altura > 1500'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Cantidad'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) as Cantidad FROM puerto WHERE altura > 1500",
                ]);
    }
    public function actionConsulta8a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Puerto::find()->select("COUNT(nompuerto) as Cantidad")->distinct()->where("altura > 1500"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Cantidad'],
            "titulo"=>"Consulta 8 con Action Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) as Cantidad FROM puerto WHERE altura > 1500",
        ]);
    }
    public function actionConsulta9(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal) > 4'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT COUNT(dorsal),  nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal) > 4",
                ]);
    }
    public function actionConsulta9a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("nomequipo")->groupBy("nomequipo")->having("COUNT(dorsal)>4"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Action Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal) > 4",
        ]);
    }
    public function actionConsulta10(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT nomequipo FROM  ciclista GROUP BY(nomequipo) HAVING COUNT(dorsal) > 4 AND "edad" BETWEEN 28 AND 32'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal) > 4 AND edad BETWEEN 28 AND 32",
                ]);
    }
    public function actionConsulta10a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("nomequipo")->groupBy("nomequipo")->having("COUNT(dorsal)>4 AND 'edad' BETWEEN 28 AND 32"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Action Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal) > 4 AND edad BETWEEN 28 AND 32",
        ]);
    }
    public function actionConsulta11(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT dorsal, COUNT(dorsal) as Victorias  FROM etapa GROUP BY dorsal'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'Victorias'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT  COUNT(dorsal)  FROM etapa GROUP BY dorsal",
                ]);
    }
    public function actionConsulta11a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Etapa::find()->select("dorsal, COUNT(dorsal) as Victorias")->groupBy("dorsal"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'Victorias'],
            "titulo"=>"Consulta 11 con Action Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT  COUNT(dorsal)  FROM etapa GROUP BY dorsal",
        ]);
    }
    public function actionConsulta12(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1",
                ]);
    }
    public function actionConsulta12a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Etapa::find()->select("dorsal")->groupBy("dorsal")->having("COUNT(dorsal)>1"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Action Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1",
        ]);
    }
}
